<?php
session_start();
$title= "Connexion SP BUDGET";
require('header.php');
require('connexionDB.php');
//test pour savoir si l'utilisateur s'est déjà identifié 
if (isset($_SESSION['connecte'])) {
  if ($_SESSION['connecte']==true) {
    $login = null;
    $password = null;
    echo '<meta http-equiv="refresh" content="0;URL=dashboard.php">';
    $error = null;
  } elseif ($_SESSION['connecte']==false) {
      $success = null;
      $login = null;
      $password = null; 
      $idUSer = null;
      $nom = null;
      $prenom = null;
      $idRole = null;
  }
} else {
  $_SESSION['connecte']=null;
  
  $error = null;
  $success = null;
  $login = null;
  $password = null; 
}

//test si login & password envoyés
if (!empty($_POST)) {
  if (isset( $_POST['login'] ) && isset($_POST['password'])) {
    $login = htmlentities($_POST['login']);
    $password = htmlentities($_POST['password']);
    //requête recherche infos utilisateurs
    $queryConnexion= "SELECT * FROM utilisateurs WHERE identifiant = '$login'";
    $requete = $connexion->query($queryConnexion);
    $requete->setFetchMode(PDO::FETCH_ASSOC);

    while($resultats=$requete->fetch()){
      if (($resultats['identifiant']===$login)&&(password_verify($password,$resultats['mdp']))) {
        $_SESSION['connecte'] = true;
        $_SESSION['idUser'] = $resultats['id_utilisateur'];
        $_SESSION['nom'] = $resultats['nom'];
        $_SESSION['prenom'] = $resultats['prenom'];
        $_SESSION['idRole'] = $resultats['id_role'];
        $_SESSION['idUserVue'] = $resultats['id_utilisateur'];
        $_SESSION['idCategorie'] = null;
        $_SESSION['idMoyenPaiement'] = null;
        $_SESSION['periodeChoisie'] = date('Y-m');
        $success = 'Vous êtes maintenant connecté, vous allez être redirigé';
        $error = null;
        echo '<meta http-equiv="refresh" content="2;URL=dashboard.php">';
      } else {
        $_SESSION['connecte'] = false;
        $success = null;
        $error = 'Il faut entrer un identifiant et un mot de passe valide';
      }
    }
    $error = (empty($success)) ? 'Il faut entrer un identifiant et un mot de passe valide' : null; //ternaire
  }
}


?>

<div class="container-fluid connexion min-vh-100 w-auto">
  <h1 class="text-center">Application Budget</h1>

  <div class="owl">
    <div class="hand"></div>
    <div class="hand hand-r"></div>
    <div class="arms">
      <div class="arm"></div>
      <div class="arm arm-r"></div>
    </div>
  </div>

  <form class="form" action="index.php" method="POST">
  <legend>Veuillez vous authentifier pour continuer</legend>
    <div class="control">
      <label for="login" class="fa fa-envelope"></label>
      <input name="login" id="login" placeholder="Identifiant" type="text" autocapitalize="none" value="<?= htmlentities($login) ?>"></input>
    </div>
    <div class="control">
      <label for="password" class="fa fa-asterisk"></label>
      <input name="password" id="password" placeholder="Mot de passe" autocapitalize="none" type="password" value="<?= htmlentities($password) ?>"></input>
    </div>
    <button class="btn btn-primary" type="submit">Connexion</button>

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $error ?>
        </div>
    <?php endif ?>

    <?php if ($success) : ?>
        <div class="alert alert-success">
            <?= $success ?>
        </div>
    <?php endif ?>
  </form>
</div>


<?php
  
  require('footer.php');
?>