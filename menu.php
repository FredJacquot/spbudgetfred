<div class="container-fluid connexion">
    <div class="d-flex justify-content-around">
        <div>
            <div class="owl">
                <div class="hand"></div>
                <div class="hand hand-r"></div>
            </div>
            <img class="billetBanque" src="./images/billetEuro.png" width="300px" alt="billet">
        </div>
        <h1 class="bd-highlight align-self-center">Application BUDGET</h1>
    </div>
</div>
<nav class="navbar navbar-light d-flex justify-content-between">
    <div class="navbar">
        <h2>Bonjour <?=mb_convert_case($_SESSION['prenom'], MB_CASE_TITLE, 'UTF-8')?></h2>  
    </div>
    <div class="navbar">
        <?php 
        $test=pathinfo($_SERVER['REQUEST_URI']);
        $pageEnCours = $test['basename'];
        $qpos = strpos($pageEnCours, "?");

        if ($qpos!==false) $pageEnCours = substr($pageEnCours, 0, $qpos);

        if ($pageEnCours=="createOperation.php"){
        ?>
        <form class="m-3" action="dashboard.php">
            <button class="btn btn-primary" type="submit">Liste des opérations</button>
        </form>
        <?php
        } else {
        ?>
        <label for="période" class="m-3">Changer de période</label>
        <div class="m-3">
            <?=select('periodeChoisie',$_SESSION['periodeChoisie'],$periodes)?>
        </div>
            <?php
            if(isset($_SESSION['idRole']) && $_SESSION['idRole']==1) {
            ?>
                <label for="utilisateur" class="m-3">Changer d'utilisateur</label> 
                <div class="m-3">
                    <?=select('utilisateurChoisi',$_SESSION['idUserVue'],$utilisateurs)?>
                </div>
            <?php
            }
            if(isset($_SESSION['idRole']) && (($_SESSION['idRole']==1)||($_SESSION['idRole']==2))) {
            ?>
                <form class="m" method="POST" action="createOperation.php">
                    <button value ="0" id="operation" name="operation" class="btn btn-success" type="submit">Ajouter une opération</button>
                </form>
            <?php
            }
        }
        ?>
                <form class="m-3" action="deconnexion.php">
            <button class="btn btn-danger" type="submit">Deconnexion</button>
        </form>
    </div>
</nav>
