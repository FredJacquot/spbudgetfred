<?php
include_once('connexionDB.php');
setlocale(LC_ALL,'fr_FR.utf8','fra');
//requête pour liste des catégories
$queryListeCategories= 'SELECT * FROM categories';
$listeCategories = $connexion->query($queryListeCategories);

while($donnees = $listeCategories->fetch(PDO::FETCH_ASSOC))
{
    $categories[$donnees['id_categorie']] = mb_convert_case($donnees['libelle_cat'], MB_CASE_TITLE, 'UTF-8');
}

$listeCategories->closeCursor();



//requête pour liste des utilisateurs
$queryListeUtilisateurs= 'SELECT * FROM utilisateurs';
$listeUtilisateurs = $connexion->query($queryListeUtilisateurs);

while($donnees = $listeUtilisateurs->fetch(PDO::FETCH_ASSOC))
{
    $utilisateurs[$donnees['id_utilisateur']] = mb_convert_case($donnees['nom'], MB_CASE_UPPER, 'UTF-8') . ' ' . mb_convert_case($donnees['prenom'], MB_CASE_TITLE, 'UTF-8');
}

$listeUtilisateurs->closeCursor();

//requête pour liste des moyens de paiement
$queryMoyensPaiement= 'SELECT * FROM moyens_paiement';
$listeMoyensPaiement = $connexion->query($queryMoyensPaiement);

while($donnees = $listeMoyensPaiement->fetch(PDO::FETCH_ASSOC))
{
    $moyensPaiement[$donnees['id_moyen']] = mb_convert_case($donnees['libelle_paiement'], MB_CASE_TITLE, 'UTF-8');
}

$listeMoyensPaiement->closeCursor();

//requête pour liste des annees et mois
$queryDateOperation= 'SELECT date_operation FROM operations GROUP BY date_operation ORDER BY date_operation DESC';
$listeDates = $connexion->query($queryDateOperation);

while($donnees = $listeDates->fetch(PDO::FETCH_ASSOC))
{
    if (!in_array(date('Y-m',strtotime($donnees['date_operation'])),$donnees)) {
        $periodes[date('Y-m',strtotime($donnees['date_operation']))] = mb_convert_case(strftime('%B %Y',strtotime($donnees['date_operation'])), MB_CASE_TITLE, 'UTF-8');
    }
}

 if (!in_array(date('Y-m'),$periodes)){
    $mois_ajout[date('Y-m')] = mb_convert_case(strftime('%B %Y',time()), MB_CASE_TITLE, 'UTF-8');
    $periodes = array_merge($mois_ajout,$periodes);
}

$listeDates->closeCursor();


function select(string $nomSelect ='', $valueSelect, array $options): string {
    $html_options = [];
    foreach ($options as $k =>$option) {
        $attributes = $k == $valueSelect ? 'selected' : '';
        $html_options[]="<option value='$k' $attributes>$option</option>";
    }

    return "<select class='form-control' id='$nomSelect' name='$nomSelect'>" . implode($html_options) . '</select>';
}


function radio(string $nomTabRadio ='', string $valueTabRadio, array $data): string {
    $attributes ='';

    if (isset($data[$nomTabRadio]) && ($valueTabRadio === $data[$nomTabRadio])) {
        $attributes .= 'checked';
    }
    
    return <<<HTML
    <input type="radio" name="$nomTabRadio" value="$valueTabRadio" $attributes>
HTML;
}

function inputText(string $nomInputText ='', string $valueInputText=''): string {

    return <<<HTML
<input type="text" class="form-control" name="$nomInputText" value="$valueInputText" step="0.01">
HTML;
}
function inputNumber(string $nomInputNumber ='', string $valueInputNumber=''): string {

        return <<<HTML
    <input type="number" class="form-control" name="$nomInputNumber" value="$valueInputNumber" step="0.01">
HTML;
}

function dump($variable)  {

    echo '<pre>';
        var_dump($variable);
    echo '</pre>';
}