<?php
require('connexionDB.php');

if(array_key_exists('deleteOperation', $_GET) and is_numeric($_GET['deleteOperation'])) {
    $idOperationASupprimer = $_GET['deleteOperation'];
} else {
	exit();
}

//requête pour supprimer une opération
$deleteOperations = $connexion->prepare('DELETE FROM operations WHERE id_operation = :idOperation ');
$deleteOperations->bindValue(':idOperation',  $idOperationASupprimer, PDO::PARAM_INT);
$deleteOperations->execute();

header("Location: dashboard.php");
?>