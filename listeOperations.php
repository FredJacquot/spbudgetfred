<?php
 if(!isset($_SESSION)){
    session_start();
    $_SESSION['connecte']= true;
} 
require('connexionDB.php');
include_once('functions.php');
//requête pour liste des opérations
$idUserVue = array_key_exists('utilisateurChoisi',$_POST) ? $_POST['utilisateurChoisi'] : $_SESSION['idUserVue'];
$_SESSION['idUserVue'] = (isset($_SESSION['idUserVue']) && $_SESSION['idUserVue']==$idUserVue) ? $_SESSION['idUserVue'] : $idUserVue;

$periodeVue = array_key_exists('periodeChoisie',$_POST) ? $_POST['periodeChoisie'] : $_SESSION['periodeChoisie'];
$_SESSION['periodeChoisie'] = (isset($_SESSION['periodeChoisie']) && $_SESSION['periodeChoisie']==$periodeVue) ? $_SESSION['periodeChoisie'] : $periodeVue;



$queryOperations = $connexion->prepare('SELECT * FROM operations WHERE id_utilisateur = :idUser AND YEAR(date_operation) = :anneeVue  AND MONTH(date_operation) = :moisVue ORDER BY date_operation DESC');
$queryOperations->bindValue(':idUser', $idUserVue ,PDO::PARAM_INT);
$queryOperations->bindValue(':anneeVue', substr($periodeVue,0,4) ,PDO::PARAM_INT);
$queryOperations->bindValue(':moisVue', substr($periodeVue,5,2) ,PDO::PARAM_INT);
$queryOperations->execute();
$operations = $queryOperations->fetchAll();

?>
<div id="operations" class="table-responsive">
    <table name='operations' class="table col-sm-12 table-bordered table-striped table-condensed cf table-hover">
        <thead class="thead-dark cf">
            <tr>
                <th>Date</th>
                <th>Catégorie</th>
                <th>Moyens de paiement</th>
                <th>Libellé opération</th>
                <th colspan="3">Montant</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <?php 
                if (empty($operations)) {
                    echo ('<td colspan="7">Pas d\'opération pour cette période</td>');
                    $queryOperations->closeCursor();
                } else {
                    foreach ($operations as $key => $value)
                    {
                    $date = DateTime::createFromFormat("Y-m-d H:i:s", $value['date_operation']);
                    $timestamp = $date->getTimestamp();
                ?>
                    <td data-title="Date"><?= htmlspecialchars(strftime("%d/%m/%Y", $timestamp))?></td>
                    <td data-title="Catégorie"><?= htmlspecialchars($categories[$value['id_categorie']])?></td>
                    <td data-title="Moyen paiement"><?= htmlspecialchars($moyensPaiement[$value['id_paiement']])?></td>
                    <td data-title="Libellé"><?= ($value['libelle_operation']==null) ? '&nbsp' : htmlspecialchars($value['libelle_operation'])?></td>
                    <td data-title="Montant"><?= htmlspecialchars($value['montant'] * $value['SENS'])?></td>
                    <?php if (isset($_SESSION['idRole']) && (($_SESSION['idRole']==1)||($_SESSION['idRole']==2))){
                    ?>
                        <td data-title="Modifier">
                            <form action="createOperation.php" type="GET">
                                <button name="updateOperation" class="btn btn-warning" type="submit" value="<?=htmlspecialchars($value['id_operation'])?>">Modifier</button>
                            </form>
                        </td>
                        <td data-title="Supprimer">
                            <form action="deleteOperation.php" type="POST">
                                <button name="deleteOperation" class="btn btn-danger" type="submit" value="<?=htmlspecialchars($value['id_operation'])?>">Supprimer</button>
                            </form>
                        </td>
                    <?php
                    }
                    ?>
            </tr>
            <?php
                }
            }
            ?>
        </tbody>
    </table>
</div>


<?php
$queryOperations->closeCursor(); // Termine le traitement de la requête

//requête pour cumul opération
$queryCumulOperations = $connexion->prepare('SELECT id_categorie, SUM(montant) AS CUMUL, SENS FROM operations WHERE id_utilisateur = :idUser AND YEAR(date_operation) = :anneeVue  AND MONTH(date_operation) = :moisVue GROUP BY id_categorie');
$queryCumulOperations->bindValue(':idUser', $idUserVue);
$queryCumulOperations->bindValue(':anneeVue', substr($periodeVue,0,4) ,PDO::PARAM_INT);
$queryCumulOperations->bindValue(':moisVue', substr($periodeVue,5,2) ,PDO::PARAM_INT);
$queryCumulOperations->execute();
$cumulOperations = $queryCumulOperations->fetchAll();
$dataPoints= array();
foreach ($cumulOperations as $key => $value){
    if($categories[$value['id_categorie']]=='Revenus'){ // On ignore les revenus 
    } else {
        $dataPoints[$key] = array("label"=> $categories[$value['id_categorie']], "y"=> $value['CUMUL'] * $value['SENS']);
    }   
}

$dataPoints=array_values($dataPoints); // On refait l'index du tableau au cas où la catégorie revenus a été ignorée
$queryCumulOperations->closeCursor(); // Termine le traitement de la requête


?>