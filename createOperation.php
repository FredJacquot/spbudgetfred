<?php
session_start();

$title = "Création d'opération";

if (isset($_SESSION['connecte']) && $_SESSION['connecte']==true) {
    require('functions.php');
    require('header.php');
    require('menu.php');

    if(array_key_exists('updateOperation', $_GET) && is_numeric($_GET['updateOperation'])) {
        $idOperationAModifier = $_GET['updateOperation'];
        $queryOperation = $connexion->prepare('SELECT * FROM operations WHERE id_operation = :idOperation');
        $queryOperation->bindValue(':idOperation',  $idOperationAModifier, PDO::PARAM_INT);
        $queryOperation->execute();
        $operationToUpdate = $queryOperation->fetchAll();

        foreach ($operationToUpdate as $key => $value)
        {
            $date = DateTime::createFromFormat("Y-m-d H:i:s", $value['date_operation']);
            $timestamp = $date->getTimestamp();
            $dateToUpdate = htmlspecialchars(strftime("%d-%m-%Y", $timestamp));
            $dateToUpdateMobile = htmlspecialchars(strftime("%Y-%m-%d", $timestamp));
            $idOperationToUpdate = htmlspecialchars($value['id_operation']);
            $categorieToUpdate = htmlspecialchars($value['id_categorie']);
            $moyenPaiementToUpdate = htmlspecialchars($value['id_paiement']);
            $userToUpdate = htmlspecialchars($value['id_utilisateur']);
            $libelleOperationToUpdate = htmlspecialchars($value['libelle_operation']);
            //$montantToupdate = str_replace('.', ',', htmlspecialchars($value['montant'])) * htmlspecialchars($value['SENS']);
            $montantToupdate = htmlspecialchars($value['montant']) * htmlspecialchars($value['SENS']);
        }
    } else {
        $dateDuJour = htmlspecialchars(date("d-m-Y"));
        $dateDuJourMobile = htmlspecialchars(date("Y-m-d"));
        $idOperationToUpdate=null;
    }
    ?>
    <div class="main container">
        <div class="row justify-content-md-center">
            <form class="col col-12 col-md-6 center" action="requetesOperation.php" method="post">
                <div class="visually-hidden">
                    <label for="idOperation" class="col-4 col-form-label">Id Opération</label>
                    <div class="col-8">
                    <input id="idOperation" type="text" name="idOperation" value="<?=(isset($idOperationToUpdate)) ? $idOperationToUpdate : 0?>"></input>
                    </div>
                </div>
            <?php $visibility = (isset($_SESSION['idRole']) && $_SESSION['idRole']==1) ? "form-group row mb-3": "visually-hidden" ;?>
                <div class="<?=$visibility?>">   
                    <label for="utilisateurs" class="col-4 col-form-label">Utilisateur</label>
                    <div class="col-8">
                        <?=select('utilisateurs',(isset($userToUpdate)) ? $userToUpdate : $_SESSION['idUserVue'],$utilisateurs)?>
                    </div>
                </div>
                <div class="form-group row mb-3 d-none d-md-flex">
                    <label for="dateOperation" class="col-4 col-form-label">Date</label>
                    <div class="col-8 input-group">
                        <input id="datepicker" type="datepicker" class="form-control"id="datepicker" name="datepicker" required value="<?=(isset($dateToUpdate)) ? $dateToUpdate : $dateDuJour?>">
                        <div class="input-group-append">
                            <span class="input-group-text fa fa-calendar"></span>
                        </div>
                    </div>
                </div>
                <div class="form-group row mb-3 d-sm-flex d-md-none">
                    <label for="dateOperation" class="col-4 col-form-label">Date</label>
                    <div class="col-8">
                        <input type="date" class="form-control"  id="datepicker2" name="datepicker2" required value="<?=(isset($dateToUpdateMobile)) ? $dateToUpdateMobile : $dateDuJourMobile?>">
                    </div>
                </div>
                <div class="form-group row mb-3">
                    <label for="categories" class="col-4 col-form-label">Catégorie</label>
                    <div class="col-8">
                        <?=select('categories',(isset($categorieToUpdate)) ? $categorieToUpdate :$_SESSION['idCategorie'],$categories)?>
                    </div>
                </div>
                <div class="form-group row mb-3">
                    <label for="moyensPaiement" class="col-4 col-form-label">Moyen de paiement</label>
                    <div class="col-8">
                        <?=select('moyensPaiement',(isset($moyenPaiementToUpdate)) ? $moyenPaiementToUpdate : $_SESSION['idMoyenPaiement'],$moyensPaiement)?>
                    </div>
                </div>
                <div class="form-group row mb-3">
                    <label for="montant" class="col-4 col-form-label">Montant</label>
                    <div class="col-8 input-group">
                        <?=inputNumber('montant',(isset($montantToupdate)) ? $montantToupdate : '')?>
                        <div class="input-group-append">
                            <span class="input-group-text">€</span>
                        </div>
                    </div>
                </div>
                <div class="form-group row mb-3">
                    <label for="sens" class="col-4 col-form-label">Libellé facultatif</label>
                    <div class="col-8">
                        <?=inputText('libelle', (isset($libelleOperationToUpdate)) ? $libelleOperationToUpdate : '')?>
                    </div>
                </div>
                <button class="btn btn-success pull-right" type="submit">Valider</button>
            </form>
        </div>
    </div>


    <?php 
    require('footer.php');
} else {
    header("Location: index.php");
}
?>

