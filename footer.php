
    <footer class="footer">
        <div class="text-center">
                <small class="copyright">Fait avec <i class="fa fa-heart" style="color:red;"></i> par F. Jacquot </br></br>LPDWCA 2020 - 2021</br>Tous droits réservés</small>
        </div>
    </footer>

</script>
<!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
<!-- Jquery JS -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<!-- Perso JS -->
    <!-- JS Chouette -->
    <script src="js/owl.js"></script>
    <!-- JS JQueryUI et Datepicker -->
    <script src="js/jquery-ui-1.12.1.custom/jquery-ui.js" ></script>
    <script type="text/javascript" src="js/datepicker.js"></script>
    <!-- JS Graphiques -->
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <script>
    window.onload = graphique();
    
    function graphique() {
    
    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        exportEnabled: true,
        title:{
            text: "Répartition des dépenses par catégorie"
        },
        subtitles: [{
            text: "Catégorie \"Revenus\" non intégrée"
        }],
        data: [{
            type: "pie",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 16,
            indexLabel: "{label} - #percent%",
            yValueFormatString: "#,##0€",
            dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
        }]
    });
    chart.render();
    }
    </script>
    <script>
    
    var datepicker2 = document.getElementById('datepicker2');
    var datepicker = document.getElementById('datepicker');
    datepicker2.addEventListener("change", function() {
      datepicker.value=datepicker2.value;
    });

    </script>
    <script>

    var utilisateurChoisi = document.getElementById("utilisateurChoisi");
    var periodeChoisie =document.getElementById("periodeChoisie");

    if(document.getElementById("utilisateurChoisi")!== null){
      utilisateurChoisi.addEventListener("change", function() {
        refreshOperation(utilisateurChoisi.value, periodeChoisie.value);
    });
    }
 
    periodeChoisie.addEventListener("change", function() {
      if(utilisateurChoisi!== null){
        refreshOperation(utilisateurChoisi.value, periodeChoisie.value);
      }
      refreshOperation(<?=$_SESSION['idUserVue']?>, periodeChoisie.value);
    });




  function refreshOperation(utilisateur, periode){
    $.ajax({
      type: "POST",
      url: 'listeOperations.php',
      data:{'utilisateurChoisi': utilisateur,
        'periodeChoisie': periode},
      dataType: 'html',
      success: function(response) {
        $('#operations').html(response);
        location.reload();
      },
      error: function(response) {
        $('#operations').html(response);
        }
      })
    }
    </script>



    
  </body>
</html>