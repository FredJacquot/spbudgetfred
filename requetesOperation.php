<?php
if(!isset($_SESSION)){
    session_start();
    $_SESSION['connecte'] = true;
} 
require('connexionDB.php');

if (isset($_SESSION['connecte']) && $_SESSION['connecte']==true) {

    var_dump($_POST);
    //traitement du formulaire
    $idOperation = $_POST['idOperation'];
    $categorie = $_POST['categories'];
    $moyenPaiement = $_POST['moyensPaiement'];
    $idUserVue = ($_SESSION['idRole']==1) ? $_POST['utilisateurs'] : $_SESSION['idUserVue'] ;
    $_SESSION['idUserVue'] = (isset($_SESSION['idUserVue']) && $_SESSION['idUserVue']==$idUserVue) ? $_SESSION['idUserVue'] : $idUserVue;
    $dateOperation = empty($_POST['datepicker']) ? $_POST['datepicker2'] : $_POST['datepicker'];
    var_dump($dateOperation);
    $libelleOperation = $_POST['libelle'];
    $montantOperation = $_POST['montant'];
    $sensOperation = $_POST['montant'] > 0 ? ($_POST['categories']==1 ? 1 : -1) : ($_POST['categories']==1 ? -1 : 1) ;

    if (is_numeric(substr($dateOperation,0,4))){
        $jour = substr($dateOperation,8,2);
        $mois = substr($dateOperation,5,2);
        $annee = substr($dateOperation,0,4);
    } else {
        $jour = substr($dateOperation,0,2);
        $mois = substr($dateOperation,3,2);
        $annee = substr($dateOperation,6,4);
    }


    $laDateMySQL = $annee . '-' . $mois . '-' . $jour;
    var_dump($laDateMySQL);
    if ($idOperation==0) {
        $operation = $connexion->prepare('INSERT INTO operations(id_categorie, id_paiement, id_utilisateur, date_operation, libelle_operation, montant, SENS)
                                                        VALUES (:idCategorie, :idPaiement, :idUtilisateur, :dateOperation, :libelleOperation, :montantOperation, :sensOperation)');
        $operation->bindValue(':idCategorie', $categorie, PDO::PARAM_INT);
        $operation->bindValue(':idPaiement', $moyenPaiement, PDO::PARAM_INT);
        $operation->bindValue(':idUtilisateur', $idUserVue, PDO::PARAM_INT);
        $operation->bindValue(':dateOperation', $laDateMySQL, PDO::PARAM_STR);
        $operation->bindValue(':libelleOperation', $libelleOperation, PDO::PARAM_STR);
        $operation->bindValue(':montantOperation', $montantOperation, PDO::PARAM_STR);
        $operation->bindValue(':sensOperation', $sensOperation, PDO::PARAM_INT);
        $operation->execute();
    } else {
        $operation = $connexion->prepare('UPDATE operations SET id_categorie = :idCategorie,
                                                                id_paiement= :idPaiement,
                                                                id_utilisateur = :idUtilisateur,
                                                                date_operation = :dateOperation,
                                                                libelle_operation = :libelleOperation,
                                                                montant = :montantOperation,
                                                                SENS = :sensOperation
                                                            WHERE operations.id_operation = :idOperation');
        $operation->bindValue(':idOperation', $idOperation, PDO::PARAM_INT);
        $operation->bindValue(':idCategorie', $categorie, PDO::PARAM_INT);
        $operation->bindValue(':idPaiement', $moyenPaiement, PDO::PARAM_INT);
        $operation->bindValue(':idUtilisateur', $idUserVue, PDO::PARAM_INT);
        $operation->bindValue(':dateOperation', $laDateMySQL, PDO::PARAM_STR);
        $operation->bindValue(':libelleOperation', $libelleOperation, PDO::PARAM_STR);
        $operation->bindValue(':montantOperation', $montantOperation, PDO::PARAM_STR);
        $operation->bindValue(':sensOperation', $sensOperation, PDO::PARAM_INT);
        $operation->execute();
    } 

    header("Location: dashboard.php");

} else {
    header("Location: index.php");
}
?>