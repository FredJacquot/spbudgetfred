<?php
session_start();

$title = "Dashboard";

if (isset($_SESSION['connecte']) && $_SESSION['connecte']==true) {
    include('functions.php');
    include('header.php');
    include('menu.php');
} else {
    header("Location: index.php");
}
?>


<div id="operationsEtGraph" class="main container">
    <div id="chartContainer" style="height: 370px; width: 100%;">
    </div> 
    <div id="listeOperations">
        <?php include('listeOperations.php');?> 
    </div>   
</div>




<?php 

require('footer.php');?>
